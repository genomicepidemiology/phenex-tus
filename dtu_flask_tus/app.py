import pika
import json
import uuid
import base64
import socket
from flask import request
from flask_tus import FlaskTus
from flask_tus.utilities import extract_metadata
from flask_tus.responses import (head_response, post_response, patch_response, delete_response, option_response)
from flask_tus.validators import (validate_head, validate_patch, validate_delete, validate_post)
from .models import Samples, Service
from .models.sample_models import Metadata
from .responses import sample_response
from .validators import validate_metadata
from .repositories import UploadRepository


class FlaskTusExtended(FlaskTus):
    pika_conn = None

    def init_app(self, app, model, db=None, repo=None):
        super(FlaskTusExtended, self).init_app(app, model, db)

        if repo:
            self.repo = repo
        else:
            self.repo = UploadRepository(model, db)

        app.config.setdefault('TUS_SAMPLE_URL', '/samples/')
        app.config.setdefault('TUS_PRESERVE_FILE_NAME', False)
        app.config.setdefault('TUS_PRESERVE_FILE_EXTENTION', False)
        app.add_url_rule(app.config['TUS_SAMPLE_URL'], 'create_sample', self.create_sample, methods=['POST'], strict_slashes=False)

    def create_sample(self):
        data = request.get_json()
        if not data:
            return sample_response(success=False, message='Error: no data in request.', data="")

        # Find sample by service information & options exact match
        service_info = {f'service__{k}__exact':v for k,v in data['serviceInfo'].items()}
        service_options = {f'service__options__{k}__exact':v for k,v in data['serviceOptions'].items()}
        sample = Samples.objects.filter(**service_info, **service_options, fingerprints=data['fingerprint'], results_email=data['resultsEmail'], anonymous_id=data['anonymous_id']).first()
        if sample is None:
            service = Service(options=data['serviceOptions'], **data['serviceInfo'], metadata=Metadata(files=[]))
            sample = Samples(service=service, fingerprints=data['fingerprint'], results_email=data['resultsEmail'], anonymous_id=data['anonymous_id'])
            sample.save()
        else:
            self.send_to_queue(sample)

        return sample_response(success=True, message='Sample metadata received.', data={"sample_id": str(sample.id)})

    def create_upload(self):
        # Get server configuration
        if request.method == 'OPTIONS':
            return option_response()

        if request.method == 'POST':
            validate_post()

            # Call callback
            self.on_create()

            # Get and typecast upload length and metadata
            upload_length = request.headers.get('Upload-Length')
            if upload_length:
                upload_length = int(upload_length)

            upload_metadata = extract_metadata(request.headers.get('Upload-Metadata'))
            if upload_metadata:
                validate_metadata(upload_metadata)

                # Get upload by fingerprint and anonymous_id
                auid = upload_metadata.get('anonymous_id')
                fingerprint = upload_metadata.get('fingerprint')
                upload = self.repo.find_by(fingerprint=fingerprint, anonymous_id=auid).first()
                if upload:
                    return post_response(upload)

            upload_path = self.generate_path(upload_metadata['filename'])

            upload = self.repo.create(anonymous_id=auid, length=upload_length, metadata=upload_metadata, path=upload_path)
            return post_response(upload)

    def modify_upload(self, upload_uuid):
        upload = self.repo.find_by_uuid(upload_uuid)

        # Get state of a resource
        if request.method == 'HEAD':
            validate_head(upload)

            if upload.offset == upload.length:
                # DTU Food usecase specific feature
                upload.add_reference_to_sample()
                sample = upload.samples[-1]
                if sample.upload_completed:
                    self.on_complete(upload)

            return head_response(upload)

        # Update a resource
        if request.method == 'PATCH':
            validate_patch(upload)

            chunk = request.data
            upload.append_chunk(chunk)
            if upload.offset == upload.length:
                # DTU Food usecase specific feature
                upload.add_reference_to_sample()

                # Call callback
                self.on_complete(upload)

            return patch_response(upload)

        if request.method == 'DELETE':
            validate_delete(upload)

            upload.delete()

            return delete_response()

    def on_complete(self, upload):
        sample = upload.samples[-1]
        self.send_to_queue(sample)

    def send_to_queue(self, sample):
        if sample.upload_completed:
            corr_id = str(uuid.uuid4())
            queue = sample.service.version
            exchange = sample.service.name

            filesname = list(self.repo.find_by(id__in=sample.uploads).values_list('filename'))

            message = json.dumps({
                'sample_id': str(sample.id),
                'files': sample.service.metadata.files,
                'filesname': filesname,
                'options': sample.service.options,
                'recipient': sample.results_email
            })

            # Sent message only if correlation_id is not set. This prevents sending duplicated messages.
            if 'correlation_id' not in sample.metadata:
                credentials = pika.PlainCredentials(self.app.config['RABBITMQ_USER'], self.app.config['RABBITMQ_PASSWORD'])
                parameters = pika.ConnectionParameters(self.app.config['RABBITMQ_HOST'], self.app.config['RABBITMQ_PORT'], self.app.config['RABBITMQ_VHOST'], credentials)
                connection = pika.BlockingConnection(parameters)
                channel = connection.channel()
                channel.queue_declare(queue, durable=True, exclusive=False)
                channel.exchange_declare(exchange=exchange, exchange_type='direct')
                channel.basic_publish(exchange=exchange, routing_key=queue, body=message, properties=pika.BasicProperties(
                    delivery_mode=2,
                    content_type='application/json',
                    correlation_id=corr_id
                ))

                sample.metadata['correlation_id'] = corr_id
                sample.save()

                print(f"[x] Host: {socket.gethostname()} Sent: '{message}' to '{queue}' queue by '{exchange}' exchange with correlation id '{corr_id}'")
