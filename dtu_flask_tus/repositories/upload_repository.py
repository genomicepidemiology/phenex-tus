from flask_tus.repositories import MongoengineRepository


class UploadRepository(MongoengineRepository):

    def create(self, *args, **kwargs):
        if "metadata" in kwargs:
            metadata = kwargs['metadata']
            if metadata.get('filename'):
                kwargs['filename'] = metadata.get('filename')
                del kwargs['metadata']['filename']
            else:
                filename = None

            if metadata.get('fingerprint'):
                kwargs['fingerprint'] = metadata.get('fingerprint')
                del kwargs['metadata']['fingerprint']
            else:
                kwargs['fingerprint'] = None

        return self.model.objects.create(**kwargs)
