from flask import current_app
from flask_tus.exceptions import TusError


def validate_metadata(metadata):
    # Check if extension is allowed
    filename = metadata.get('filename')

    # If filename or extension rules are not set
    if not filename or not current_app.config.get('TUS_EXTENSION_WHITELIST') or not current_app.config.get('TUS_EXTENSION_BLACKLIST'):
        return

    if current_app.config.get('TUS_EXTENSION_WHITELIST'):
        if get_extension(filename) not in current_app.config.get('TUS_EXTENSION_WHITELIST'):
            raise TusError(406, 'Extensions not allowed')
    else:
        if get_extension(filename) in current_app.config.get('TUS_EXTENSION_BLACKLIST'):
            raise TusError(406, 'Extensions not allowed')


def get_extension(filename, depth=1):
    return '.'.join(filename.split('.')[depth:])
