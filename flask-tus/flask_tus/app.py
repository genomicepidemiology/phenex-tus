import os
import time
import uuid
import tempfile
import datetime
from flask import request
from .utilities import extract_metadata
from .exceptions import TusError
from .repositories import Repo
from .responses import (option_response, post_response, head_response, patch_response, delete_response)
from .validators import (validate_post, validate_head, validate_patch, validate_delete)


class FlaskTus(object):
    def __init__(self, app=None, model=None, db=None, repo=None):
        if app:
            self.init_app(app, model, db, repo)

    # Application factory
    def init_app(self, app, model, db=None, repo=None):
        self.app = app

        app.config.setdefault('TUS_UPLOAD_DIR', tempfile.mkdtemp())
        app.config.setdefault('TUS_UPLOAD_URL', '/files/')
        app.config.setdefault('TUS_MAX_SIZE', 2**32)  # 4 GB
        app.config.setdefault('TUS_EXPIRATION', datetime.timedelta(days=1))
        app.config.setdefault('TUS_CHUNK_SIZE', 2**16)  # 8 KB
        app.config.setdefault('TUS_PRESERVE_FILE_NAME', False)
        app.config.setdefault('TUS_PRESERVE_FILE_EXTENTION', False)

        app.register_error_handler(TusError, TusError.error_handler)

        app.add_url_rule(app.config['TUS_UPLOAD_URL'], 'create_upload', self.create_upload, methods=['OPTIONS', 'POST'], strict_slashes=False)
        app.add_url_rule(app.config['TUS_UPLOAD_URL'].rstrip('//') + '/<upload_uuid>', 'modify_upload', self.modify_upload, methods=['HEAD', 'PATCH', 'DELETE'], strict_slashes=False)

        if repo:
            self.repo = repo
        else:
            # Repository factory
            self.repo = Repo(model, db)

        # Inject flask_tus as property to support CLI commands
        app.flask_tus = self

    def create_upload(self):
        # Get server configuration
        if request.method == 'OPTIONS':
            return option_response()

        if request.method == 'POST':
            validate_post()

            # Call callback
            self.on_create()

            # Get and typecast upload length and metadata
            upload_length = request.headers.get('Upload-Length')
            if upload_length:
                upload_length = int(upload_length)

            upload_metadata = extract_metadata(request.headers.get('Upload-Metadata'))

            upload_path = self.generate_path(upload_metadata['filename'])

            upload = self.repo.create(length=upload_length, metadata=upload_metadata, path=upload_path)

            return post_response(upload)

    def modify_upload(self, upload_uuid):
        upload = self.repo.find_by_uuid(upload_uuid)

        # Get state of a resource
        if request.method == 'HEAD':
            validate_head(upload)
            return head_response(upload)

        # Update a resource
        if request.method == 'PATCH':
            validate_patch(upload)

            chunk = request.data
            upload.append_chunk(chunk)

            if upload.offset == upload.length:
                self.on_complete()

            return patch_response(upload)

        if request.method == 'DELETE':
            validate_delete(upload)

            upload.delete()

            return delete_response()

    def generate_path(self, filename):
        if self.app.config['TUS_PRESERVE_FILE_NAME']:
            file_name = filename
        else:
            file_name = str(uuid.uuid4())

        if self.app.config['TUS_PRESERVE_FILE_EXTENTION'] and not self.app.config['TUS_PRESERVE_FILE_NAME']:
            fname = filename.split('.')
            if len(fname) == 2:
                extention = str.join('.', fname[-1:])

            if len(fname) == 3:
                extention = str.join('.', fname[-2:])

            file_name = f'{file_name}.{extention}'

        upload_dir = time.strftime(self.app.config['TUS_UPLOAD_DIR'])
        os.makedirs(upload_dir, exist_ok=True)

        return os.path.join(upload_dir, file_name)

    def on_create(self):
        # Callback for creation of upload
        pass

    def pre_save(self):
        # Callback for pre-save on each chunk
        pass

    def post_save(self):
        # Callback for post-save on each chunk
        pass

    def on_complete(self):
        # Callback for completion of upload
        pass

    def pre_delete(self):
        # Callback for pre-delete of upload
        pass

    def post_delete(self):
        # Callback for post-delete of upload
        pass
